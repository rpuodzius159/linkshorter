# Linkshorter

## Getting started

How to launch project:

- [ ] ```git clone https://gitlab.com/rpuodzius159/linkshorter.git```
- [ ] ```cd linkshorter/```
- [ ] setup .env file
- [ ] ```docker-compose up --build -d```
- [ ] ```composer install```
- [ ] From docker app container launch: ```php artisan migrate --seed```
- [ ] go to http://localhost
- test user:
    - test@gmail.com:simple-pass
