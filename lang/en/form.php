<?php

declare(strict_types=1);

return [
    "login_form_title" => "Login Form",
    "login_form_submit_button" => "Submit",
    "password_label" => "Password",
    "email_label" => "Email",
    "login_form_register_button" => "Register",
    "password_confirm_label" => "Repeat password",
    "login_form_register_title" => "Registration form",
    "main_title" => "Input your url what needs to be shorten",
    "main_button_title" => "Do it !",
    "url_title" => "Input Url",
    "url_list" => "You links",
    "no_links_found" => "You dont have any links, create new one",
    "link_share_text" => "Share:",
    "logout_link" => "Logout",
    "login_link" => "Login",
    "register_link" => "Register",
    "back_link" => "Back",
    "email" => [
        'shared_text' => "Hey!, Someone shared with you link: :link",
        'broken_link_text' => "Hey!, This link will be deleted, cuz its broken: :link"
    ],
    "url" => [
        'required' => 'Url is required',
        'incorrect' => 'Incorrect url (url must start with http:// or https://'
    ]
];
