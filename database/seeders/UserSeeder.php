<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    private const DEFAULT_PASSWORD = 'simple-pass';
    private const DEFAULT_EMAIl = 'test@gmail.com';

    public function run(): void
    {
        User::create(
            [
                'email' => self::DEFAULT_EMAIl,
                'password' => Hash::make(self::DEFAULT_PASSWORD)
            ]
        );
    }
}
