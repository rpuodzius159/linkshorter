<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Analytic;
use App\Models\GuestsLinks;
use App\Models\Links;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class LinksSeeder extends Seeder
{
    public function run(): void
    {
        $sampleData = [
            [
                'original_url' => 'https://laracasts.com/series/phpstorm-for-laravel-developers',
                'short_url' => Str::random(10)
            ],
            [
                'original_url' => 'https://laravel.com/docs/10.x/helpers',
                'short_url' => Str::random(10)
            ],
            [
                'original_url' => Links::DEFAULT_TEST_LINK,
                'short_url' => Str::random(10)
            ],
        ];

        foreach ($sampleData as $data) {
            $link = Links::create(
                $data
            );

            User::first()->links()->attach($link);
        }

        GuestsLinks::create(
            [
                'guest_id' => Str::random(),
                'link_id' => $link->id
            ]
        );

        Analytic::create(
            [
                'link_id' => $link->id,
                'visits' => 99
            ]
        );
    }
}
