<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('guests_links', function (Blueprint $table): void {
            $table->id();
            $table->string('guest_id')->index();
            $table->foreignUuid('link_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('guests_links', function (Blueprint $table): void {
            $table->dropConstrainedForeignId('link_id');
        });

        Schema::dropIfExists('guests_links');
    }
};
