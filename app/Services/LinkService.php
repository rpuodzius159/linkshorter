<?php

declare(strict_types=1);

namespace App\Services;

use App\DTO\Links\CreateLinkDTO;
use App\DTO\Links\ShareLinkDTO;
use App\Mail\SharedLink;
use App\Models\GuestsLinks;
use App\Models\Links;
use App\Models\User;
use App\Repositories\LinkRepository;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class LinkService
{
    public function __construct(
        private LinkRepository $linkRepository,
        private UserRepository $userRepository,
    ) {
    }

    public function getLink(string $id): ?Links
    {
        return Links::find($id);
    }

    public function getLinks(): ?Collection
    {
        if ( ! Auth::guest()) {
            return $this->getUserLinks();
        }

        return $this->getGuestLinks();
    }

    public function shareLinkWithUser(ShareLinkDTO $linkDTO): void
    {
        $link = Links::find($linkDTO->link_id);
        $user = $this->userRepository->findUserByEmaiL($linkDTO->email);
        if ($user && $link) {
            $user->links()->syncWithoutDetaching($link);
            Mail::to($user)->send(new SharedLink($link));
        }
    }

    public function storeLinkAndAttachUser(CreateLinkDTO $link): void
    {
        $link = $this->linkRepository->storeLink($link);

        if ( ! Auth::guest()) {
            Auth::user()->links()->syncWithoutDetaching($link);
        } else {
            $userId = Cookie::get('guestId', session('guestId'));

            if ( ! $userId) {
                $userId = Str::random();
                session(['guestId', $userId]);
                Cookie::queue('guestId', $userId);
            }

            GuestsLinks::firstOrCreate([
                'guest_id' => $userId,
                'link_id' => $link->id,
            ]);
        }
    }

    public function getLinkRedirectUrl(string $shortUrl): string
    {
        $link = $this->linkRepository->findLinkByShortUrl($shortUrl);

        return $link ? Url::to($link->original_url) : Url::route('home');
    }

    public function syncGuestLinksToUser(User $user): void
    {
        $links = $this->getGuestLinks();
        if ($links) {
            $user->links()->detach($links);
            $user->links()->attach($links);
        }
    }

    private function getUserLinks(): Collection
    {
        return Auth::user()->links()->withTrashed()->get();
    }

    private function getGuestLinks(): ?Collection
    {
        $guestId = Cookie::get('guestId', session('guestId'));

        if ($guestId) {
            return $this->linkRepository->getGuestLinks($guestId);
        }

        return null;
    }

}
