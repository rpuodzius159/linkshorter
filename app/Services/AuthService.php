<?php

declare(strict_types=1);

namespace App\Services;

use App\DTO\Auth\AuthCredentialsDTO;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    public function __construct(
        private UserRepository $userRepository,
        private LinkService $linkService,
    ) {
    }

    public function registerUser(AuthCredentialsDTO $credentials): bool
    {
        $user = $this->userRepository->store($credentials);

        if ($user) {
            $this->linkService->syncGuestLinksToUser($user);
            Auth::login($user);

            return true;
        }

        return false;
    }

    public function loginUser(AuthCredentialsDTO $credentials): bool
    {
        return ! Auth::guest() || Auth::attempt($credentials->toArray());
    }
}
