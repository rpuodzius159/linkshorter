<?php

declare(strict_types=1);

namespace App\Http\Requests\Link;

use Illuminate\Foundation\Http\FormRequest;

class CreateLink extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'url' => 'required|url:http,https'
        ];
    }

    public function messages(): array
    {
        return [
            'url.required' => __('form.url.required'),
            'url.url' => __('form.url.incorrect'),
        ];
    }
}
