<?php

declare(strict_types=1);

namespace App\Http\Requests\Link;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ShareLink extends FormRequest
{
    public function authorize(): bool
    {
        return ! Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'link_id' => 'required|uuid|exists:links,id',
            'email' => 'required|email|exists:users,email'
        ];
    }
}
