<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class CreateGuestIdIfNotExists
{
    public function handle(Request $request, Closure $next): Response
    {
        if ( ! Auth::check() && null === Cookie::get('guestId', session('guestId'))) {
            $guestId = Str::random();
            session(['guestId' => $guestId]);
            Cookie::queue('guestId', $guestId);
        }

        return $next($request);
    }

}
