<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\DTO\Auth\AuthCredentialsDTO;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Services\AuthService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function __construct(private AuthService $authService)
    {
    }

    public function registerForm(): Response
    {
        return response()->view('register');
    }

    public function loginForm(): Response|RedirectResponse
    {
        if ( ! Auth::guest()) {
            return response()->redirectToRoute('home');
        }

        return response()->view('login');
    }

    public function registerUser(RegisterRequest $request): Response|RedirectResponse
    {
        $data = $request->validated();

        if ($this->authService->registerUser(new AuthCredentialsDTO($data))) {
            return response()->redirectToRoute('home');
        }

        return response()->view('register');
    }

    public function loginUser(LoginRequest $request): Response|RedirectResponse
    {
        $data = $request->validated();

        if ($this->authService->loginUser(new AuthCredentialsDTO($data))) {
            return response()->redirectToRoute('home');
        }

        return response()->view('login');
    }

    public function logout(): RedirectResponse
    {
        Auth::logout();

        return response()->redirectToRoute('home');
    }

}
