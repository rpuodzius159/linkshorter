<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\DTO\Links\CreateLinkDTO;
use App\DTO\Links\ShareLinkDTO;
use App\Http\Requests\Link\CreateLink;
use App\Http\Requests\Link\ShareLink;
use App\Services\LinkService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;

class LinksController extends Controller
{
    public function __construct(private LinkService $linkService)
    {
    }

    public function index(): Response
    {
        $links = $this->linkService->getLinks();

        return response()->view('home', ['links' => $links]);
    }

    public function store(CreateLink $request): RedirectResponse
    {
        $data = $request->validated();

        $this->linkService->storeLinkAndAttachUser(new CreateLinkDTO($data['url']));

        return response()->redirectToRoute('home');
    }

    public function resolveLink(string $link): RedirectResponse
    {
        $redirectUrl = $this->linkService->getLinkRedirectUrl($link);

        return response()->redirectTo($redirectUrl);
    }

    public function shareLinkForm(string $linkId): RedirectResponse|Response
    {
        $link = $this->linkService->getLink($linkId);

        if ($link) {
            return response()->view('share_link', ['link' => $link]);
        }

        return response()->redirectToRoute('home');
    }

    public function shareLink(ShareLink $request): RedirectResponse
    {
        $data = $request->validated();

        $this->linkService->shareLinkWithUser(new ShareLinkDTO($data));

        return response()->redirectToRoute('home');
    }
}
