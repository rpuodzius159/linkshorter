<?php

declare(strict_types=1);

namespace App\DTO\Links;

use Illuminate\Support\Str;

class CreateLinkDTO
{
    public string $original_url;
    public string $short_url;

    public function __construct(string $original_url)
    {
        $this->original_url = $original_url;
        $this->short_url = Str::random(10);
    }
}
