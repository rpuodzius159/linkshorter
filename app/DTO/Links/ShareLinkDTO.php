<?php

declare(strict_types=1);

namespace App\DTO\Links;

class ShareLinkDTO
{
    public string $email;
    public string $link_id;

    public function __construct($arguments)
    {
        $this->email = $arguments['email'];
        $this->link_id = $arguments['link_id'];
    }
}
