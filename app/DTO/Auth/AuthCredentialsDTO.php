<?php

declare(strict_types=1);

namespace App\DTO\Auth;

class AuthCredentialsDTO
{
    public string $email;
    public string $password;

    public function __construct($arguments)
    {
        $this->email = $arguments['email'];
        $this->password = $arguments['password'];
    }

    public function toArray(): array
    {
        return [
            'email' => $this->email,
            'password' => $this->password,
        ];
    }
}
