<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Mail\LinkBroken;
use App\Models\Links;
use Exception;
use Http;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CheckLinksAndNotifyUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:check-links-and-notify-users {chunkSize=5}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks links and notify users if they are not acceptable';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $chunkSize = $this->argument('chunkSize');
        $linksList = Links::all('original_url', 'id')->chunk($chunkSize);

        foreach ($linksList as $chunkList) {
            foreach ($chunkList as $link) {
                $failedUrl = false;
                try {
                    $result = Http::get($link->original_url);
                } catch (Exception $e) {
                    $failedUrl = true;
                }

                if ($result->failed() || 404 === $result->status() || $failedUrl) {
                    foreach ($link->users as $user) {
                        Mail::to($user)
                            ->queue(new LinkBroken($link));
                    }

                    $link->delete();
                }
            }
            sleep(1); //giving rest for cpu
        }
    }
}
