<?php

declare(strict_types=1);

namespace App\Repositories;

use App\DTO\Auth\AuthCredentialsDTO;
use App\Models\User;

class UserRepository
{
    public function store(AuthCredentialsDTO $credentialsDTO): User
    {
        return User::create($credentialsDTO->toArray());
    }

    public function findUserByEmaiL(string $email): ?User
    {
        return User::where('email', '=', $email)->first();
    }
}
