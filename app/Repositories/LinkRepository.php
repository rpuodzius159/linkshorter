<?php

declare(strict_types=1);

namespace App\Repositories;

use App\DTO\Links\CreateLinkDTO;
use App\Models\GuestsLinks;
use App\Models\Links;
use Illuminate\Database\Eloquent\Collection;

class LinkRepository
{
    public function getAllLinks(): Collection
    {
        return Links::all();
    }

    public function getGuestLinks(string $guestId): Collection
    {
        $linksId = GuestsLinks::where('guest_id', '=', $guestId)->get()?->pluck('link_id')->toArray();

        return Links::whereIn('id', $linksId)->withTrashed()->get();
    }

    public function findLinkByShortUrl(string $shortUrl): ?Links
    {
        return Links::where('short_url', '=', $shortUrl)->first();
    }

    public function storeLink(CreateLinkDTO $link): Links
    {
        return Links::firstOrCreate(
            ['original_url' => $link->original_url],
            ['short_url' => $link->short_url]
        );
    }
}
