<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;

class Links extends Model
{
    use HasFactory;
    use HasUuids;
    use SoftDeletes;

    public const DEFAULT_TEST_LINK = 'https://SomeRandomNotExistingUrlFOrtest.com';


    protected $fillable = [
        'original_url',
        'short_url',
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'users_links', 'link_id');
    }

    public function analytic(): HasOne
    {
        return $this->hasOne(Analytic::class, 'link_id');
    }

    public function guests(): HasMany
    {
        return $this->hasMany(GuestsLinks::class, 'link_id');
    }

    public function getLinkHref(): string
    {
        if ($this->trashed()) {
            return $this->short_url;
        }

        return sprintf(
            '<a href="%s">%s</a>',
            Url::route('resolveLink', [$this->short_url]),
            Url::route('resolveLink', [$this->short_url])
        );
    }
}
