<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Analytic extends Model
{
    use HasFactory;

    protected $fillable = [
        'visits',
        'password',
    ];

    public function link(): HasOne
    {
        return $this->hasOne(Links::class);
    }

}
