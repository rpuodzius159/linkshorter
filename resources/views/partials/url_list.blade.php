@if (!is_null($links) && !empty($links))
    <div id="tasks" class="my-5">
        @foreach($links as $link)
            <div id="task" class="flex w-full items-center border-b border-slate-200 border-l-4 border-l-transparent">
                <div class="inline-flex items-center space-x-2">
                    <div class="text-slate-500 @if($link->trashed()) line-through @endif">
                        {{$link->original_url}} -> {!! $link->getLinkHref() !!}
                        @auth
                            <a href="{{ route('shareLinkForm', ['linkId' => $link]) }}"
                               class="max-h-24" alt="Share {{$link->original_url}}">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor"
                                     class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="M21.75 9v.906a2.25 2.25 0 01-1.183 1.981l-6.478 3.488M2.25 9v.906a2.25 2.25 0 001.183 1.981l6.478 3.488m8.839 2.51l-4.66-2.51m0 0l-1.023-.55a2.25 2.25 0 00-2.134 0l-1.022.55m0 0l-4.661 2.51m16.5 1.615a2.25 2.25 0 01-2.25 2.25h-15a2.25 2.25 0 01-2.25-2.25V8.844a2.25 2.25 0 011.183-1.98l7.5-4.04a2.25 2.25 0 012.134 0l7.5 4.04a2.25 2.25 0 011.183 1.98V19.5z"/>
                                </svg>
                            </a>
                        @endauth
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@else
    {{ __('form.no_links_found') }}
@endif

