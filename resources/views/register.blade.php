@extends('layouts.app')

@section('content')
    <!-- component -->
    <div class="min-h-screen bg-gray-100 py-6 flex flex-col justify-center sm:py-12">
        <div class="relative py-3 sm:max-w-xl sm:mx-auto">
            <div
                class="absolute inset-0 bg-gradient-to-r from-blue-300 to-blue-600 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl">
            </div>
            <div class="relative px-4 py-10 bg-white shadow-lg sm:rounded-3xl sm:p-20">
                <div class="max-w-md mx-auto">
                    <div>
                        <h1 class="text-2xl font-semibold">{{ __('form.login_form_register_title') }}</h1>
                    </div>
                    <form action="{{ route('registerUser') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="divide-y divide-gray-200">
                            <div class="py-8 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
                                <div class="relative">
                                    <input autocomplete="off" id="email" name="email" type="text"
                                           class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                                           placeholder="{{ __('form.email_label') }}"/>
                                    <label for="email"
                                           class="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm">{{ __('form.email_label') }}</label>
                                    {{ $errors->first('email') }}
                                </div>
                                <div class="relative">
                                    <input autocomplete="off" id="password" name="password" type="password"
                                           class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                                           placeholder="{{ __('form.password_label') }}"/>
                                    <label for="password"
                                           class="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm">{{ __('form.password_label') }}</label>
                                    {{ $errors->first('password') }}
                                </div>
                                <div class="relative">
                                    <input autocomplete="off" id="password_confirmation" name="password_confirmation" type="password"
                                           class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600"
                                           placeholder="{{ __('form.password_confirm_label') }}"/>
                                    <label for="password_confirmation "
                                           class="absolute left-0 -top-3.5 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:-top-3.5 peer-focus:text-gray-600 peer-focus:text-sm">{{ __('form.password_confirm_label') }}</label>
                                    {{ $errors->first('password_confirmation') }}
                                </div>
                                <div class="relative">
                                    <button type='submit'
                                            class="bg-blue-500 text-white rounded-md px-2 py-1">{{ __('form.login_form_register_button') }}</button>
                                    <a href="{{ route('home') }}"
                                       class="px-6 py-3 text-blue-100 no-underline bg-blue-500 rounded hover:bg-blue-600 hover:underline hover:text-blue-200">
                                        {{ __('form.back_link') }}</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
