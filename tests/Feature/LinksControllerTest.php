<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Models\GuestsLinks;
use App\Models\Links;
use Database\Seeders\LinksSeeder;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class LinksControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function test_index_should_show_guests_links(): void
    {
        $this->seed([UserSeeder::class, LinksSeeder::class]);

        $guestId = GuestsLinks::first()->guest_id;
        $guestLinks = Links::whereRelation('guests', 'guest_id', $guestId)->get();
        $response = $this->withCookie('guestId', $guestId)->get('/');
        $response->assertViewHas('links', $guestLinks);
    }

    public function test_index_should_not_show_guests_links(): void
    {
        $this->seed([UserSeeder::class, LinksSeeder::class]);

        $response = $this->get('/');
        $response->assertViewHas('links', null);
    }

    public function test_store_should_throw_valdiation_error(): void
    {
        $this->seed([UserSeeder::class, LinksSeeder::class]);

        $response = $this->post('/shorten-link', ['url' => 'SomerandomLink.com']);
        $response->assertInvalid();
    }

    public function test_store_should_store_guest_url(): void
    {
        $this->seed([UserSeeder::class, LinksSeeder::class]);

        $guestId = Str::random();

        $response = $this->withCookie('guestId', $guestId)->post('/shorten-link', ['url' => 'https://GG.com']);
        $this->assertTrue(1 === GuestsLinks::where('guest_id', $guestId)->count());
        $response->assertRedirectToRoute('home');
    }

    public function test_store_should_not_dublicate_same_url_for_guest(): void
    {
        $this->seed([UserSeeder::class, LinksSeeder::class]);

        $guestId = Str::random();

        $this->withCookie('guestId', $guestId)->post('/shorten-link', ['url' => 'https://GG.com']);

        $this->withCookie('guestId', $guestId)->post('/shorten-link', ['url' => 'https://GG2.com']);

        $response = $this->withCookie('guestId', $guestId)->post('/shorten-link', ['url' => 'https://GG.com']);

        $this->assertTrue(2 === GuestsLinks::where('guest_id', $guestId)->count());
        $response->assertValid();
    }

    public function test_store_should_store_if_missing_cookie(): void
    {
        $this->seed([UserSeeder::class, LinksSeeder::class]);

        $response = $this->post('/shorten-link', ['url' => 'https://GG.com']);

        $response->assertValid();
    }

    public function test_store_should_not_dublicate_links_entries_for_guest(): void
    {
        $this->seed([UserSeeder::class, LinksSeeder::class]);
        $guestId = Str::random();

        $linksCountBefore = Links::all()->count();
        $this->withCookie('guestId', $guestId)->post('/shorten-link', ['url' => 'https://GG.com']);

        $this->withCookie('guestId', $guestId)->post('/shorten-link', ['url' => 'https://GG2.com']);

        $this->withCookie('guestId', $guestId)->post('/shorten-link', ['url' => 'https://GG.com']);

        $this->withCookie('guestId', $guestId)->post('/shorten-link', ['url' => Links::DEFAULT_TEST_LINK]);

        $linksCountAfter = Links::all()->count();
        $this->assertEquals($linksCountBefore + 2, $linksCountAfter);
    }
}
