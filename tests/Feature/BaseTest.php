<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Models\Links;
use Database\Seeders\LinksSeeder;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BaseTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function test_check_relations_work(): void
    {
        $this->seed([UserSeeder::class, LinksSeeder::class]);
        $link = Links::where('original_url', '=', Links::DEFAULT_TEST_LINK)->get()->first();

        $this->assertModelExists($link);
        $this->assertTrue($link->guests()->exists());
        $this->assertTrue($link->users()->exists());
        $this->assertTrue(3 === $link->users()->oldest('id')->first()->links()->count());
        $this->assertTrue($link->analytic->visits >= 99);
    }
}
