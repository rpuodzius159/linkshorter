<?php

declare(strict_types=1);

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LinksController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::controller(AuthController::class)->group(function (): void {
    Route::get('/login', 'loginForm')->name('loginForm');
    Route::post('/login', 'loginUser')->name('loginUser');
    Route::get('/logout', 'logout')->name('logout');
    Route::get('/register', 'registerForm')->name('registerForm');
    Route::Post('/register', 'registerUser')->name('registerUser');
});
Route::controller(LinksController::class)->group(function (): void {
    Route::get('/', 'index')->name('home');
    Route::post('/shorten-link', 'store')->name('storeLink');
    Route::get('/share/{linkId}', 'shareLinkForm')->name('shareLinkForm');
    Route::post('/share', 'shareLink')->name('shareLink');
    Route::get('/{link}', 'resolveLink')->name('resolveLink');
});
